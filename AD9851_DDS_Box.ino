//
// M.Giebler
// 2014-07
// A simple signal generator using an AD9851 DDS and Arduino.
// Frequency range: 1 to 180Mhz.  (70Mhz max usable)
//  Revision history further down.
//
// Programmable attenuator:
//	https://adauratech.com/product/ad-usb1ar36g95/
//	https://www.minicircuits.com/WebStore/RF-Programmable-Step-Attenuators.html
//
// Arduino has lots of ready made code examples,
// but Arduino underlying HAL is bloatware slow.
//   e.g. digitalRead/Write() very slow.
//  See here for direct IO: http://www.hobbytronics.co.uk/arduino-tutorial4-cylon
//
// Board type: Arduino Uno

// -----------------------------------------------------
// Mark's debug macro
// -----------------------------------------------------
// 		Put any debug only code inside the parenthesis (represented by 'a') in below macro.
//  	'a' can be multiple statements on multiple lines.
// 		comment out next line to turn off all debug code.
#define G_DEBUG(a)  {a;}
#ifndef G_DEBUG
// 		if G_DEBUG() is commented out above, then define NULL debug macro that throws code 'a' away.
#define G_DEBUG(a) {;;;}
#endif
// 		use this macro to temporarily disable an individual G_DEBUG() macro group of code by renaming it to this:
#define G_DEBUG_N(a) {;;;}
// -----------------------------------------------------
// END -- Mark's debug macro
// -----------------------------------------------------

// ______________Serial port baudrate________________
#define BAUDRATE	230400

// ------------------------------------------------------------
//  EEPROM definitions
// ------------------------------------------------------------
#include <EEPROM.h>

#define EEPROM_SIGNATURE_VAL		0xa3	// To determine if EEPROM layout has been init'ed.
#define EEPROM_DATA_VERSION			0x04	// The EEPROM layout version.

// EEPROM locations:
#define EEPROM_SIGNATURE_B1			( 4 )	// signature byte
#define EEPROM_SIGNATURE_B2			( 5 )	// version number byte
#define EEPROM_DATA_SIZE			( 6 )
#define EEPROM_BCKLIGHT_BARS		( 8 )
#define EEPROM_OSC_CALIBRATION_B2	( 9 )
#define EEPROM_OSC_CALIBRATION_B1	( 10 )

// ------------------------------------------------------------
// hardware summary:
// Arduino Uno Digital Pins used:
// ------------------------------------------------------------
//	LCD:  11, 12, 16 (A2), 17 (A3), 18 (A4), 19 (A5)
//	LCD LED:  6 (PWM)
//	DDS: 7, 8, 9, 10
//	LED: 13
//  Rotary Encoder: 2, 3, 4
//  UART: 0, 1
//
// Arduino Uno Pins available:
// ----------------------------
//	0, 1,  (if UART is not used)
//	5
//	Analog pins (AI): (Digital pin # DI)
//	AI 0,   1,
//	DI 14, 15,
//

/*
  Arduino LCD Class: http://arduino.cc/en/Reference/LiquidCrystal

  The LCD interface pins:

 * LCD RS pin to 	digital pin 12
 * LCD Enable pin to digital pin 11
 * LCD D4 pin to 	digital pin 5 -> 16 (A2)
 * LCD D5 pin to 	digital pin 4 -> 17 (A3)
 * LCD D6 pin to 	digital pin 3 -> 18 (A4)
 * LCD D7 pin to 	digital pin 2 -> 19 (A5)
 * LCD R/W pin to 	ground
 * 10K potentiometer:
 * 	outside ends to +5V and ground
 * 	LCD VO pin 3 - to center wiper
*/
#include <LiquidCrystal.h>  // the LCD library
// initialize the library with the numbers of the interface pins
LiquidCrystal lcd(12, 11, 16, 17, 18, 19);
#define BCKLIGHT_PWM_PIN	6	/* LED backlight PWM pin */

// -------------------------------------------------------------------
// define the AD9851 DDS  circuit interface:

// define DDS interface pins
#define DDS_RESET	7	// MjG:  Add a reset pin.  Wow!
#define DDS_LOAD	8
#define DDS_CLOCK	9
#define DDS_DATA	10	// D7 is serial in on DDS board.

// Uno LED pin
#define LED			13	// UNO board LED

// Define rotary encoder interface pins.
#define ROT_ENCODER_PHASE_A	2
#define ROT_ENCODER_PHASE_B	3
#define ROT_ENCODER_CLICK	4 /* v0.11 was A3 */

#define LCD_CURSOR_COL__FREQ_1HZ	10	/* LCD column location for our cursor */
#define LCD_CURSOR_COL__1Bth		12	/* column for 1Bth digit in calibration number */

// -------------------------------------------------------------------
// Global vars
const char version[]={"1.01"};

// v1.01  2022-12-05
//		- Fix Calibration and Exit not showing in menu item list after adding issue #1 feature.
//		  Resolves issue #2
//		- Expanded Preset frequency list.
//
// v1.00  2022-09-30
//		- Add preset frequencies in the menu system.
//		  Resolves issue #1
//
// v0.19  2022-09-05
//		- Changed Baudrate to 230400
//
// v0.18  2022-07-13
//		- Changed behavior when end of range is hit when changing frequency in large steps.
//		No longer jumps to MAX or MIN frequency, but keeps frequency unchanged.
//		For Example; if incrementing the MHz position and frequency was 69.800 MHz
//		previously would jump to 70.000MHz, now it will stay at 69.800 MHz
//		and thus the lower digits now do not change.
//		- Change power on frequency from 32MHz to 10MHz (startFreq)
//
// v0.17  2014-10-31
//		Functionality complete.
//		change freq step size change to Click_UP event instead of DOWN.
//		change freq display units from Hz to KHz.
//		added debounce to encoder ISR state machine on phase a or b going low.
//		add calibrate DDS ref clock freq menu and save to EEPROM.
//
// v0.16  2014-10-24
//		Experimenting with 64 bit math on this AVR chip.  (takes 4k just to support one line of code!)
//		Code Size: 14,868 bytes (of a 32,256 byte maximum) - 46% used
//		Results; 46b value delta one count vs 32b value: (with Ref osc +3600Hz above 180MHz.)
//			32b: Freq: 4000034	-> DDS N: 95442619
//			64b: Freq: 4000034	-> DDS N: 95442620
//
// v0.15  2014-10-23
//		Adding in EEPROM to save backlight level, and calibration value.
//		Backlight EEPROM save and restore works.
//		Code Size: 10,742 bytes (of a 32,256 byte maximum) - 33% used
//
// v0.14  2014-10-19
//		Implements start of menu system part of state machine.
//		Upper menu items complete.
//		Lower menu Only backlight is complete.
//		Fixed stack overrun bug which caused weird LCD issues.
//
// v0.13  2014-10-18  Start State Machine feature.  Works same as v0.12 but using state machine.
//
// v0.12  2014-10-17  Re-ordered LCD pins and Encoder button pin.
//		Added version display.
//
// v0.11  2014-10-16 Added LCD cursor to indicate the frequency step position. Moves with each encoder push button click.
//		Simple frequency calibration by adding in an offset.  Doesn't work for the entire range.
//
// v0.10  2014-10-10 Encoder click button changes step rate; cycles thru 1Hz, 100Hz, 1KHz, 100KHz, 1MHz
//		Changed to my fractional Frequency to N converter - frequencyToDDSCount().  Now F to N conversion is right on!
//
// Previously N was calculated with formula, but the size of the numbers overflowed a long
//  (This was from code people were using for this DDS - amazingly stupid.)
// Example of wrong N counts for 1MHz steps:
/*
0: Freq: 1000000	-> DDS N: 23860400
0: Freq: 2000000	-> DDS N: 47720800
0: Freq: 4000000	-> DDS N: 95441600
0: Freq: 6000000	-> DDS N: 143162400
0: Freq: 8000000	-> DDS N: 190883200
0: Freq: 10000000	-> DDS N: 238603984
0: Freq: 15000000	-> DDS N: 357905984
0: Freq: 20000000	-> DDS N: 477207968
0: Freq: 22000000	-> DDS N: 524928768
0: Freq: 24000000	-> DDS N: 572649600
0: Freq: 24000001	-> DDS N: 572649600	<-- no change in N!
0: Freq: 24000002	-> DDS N: 572649600	<-- no change in N!
0: Freq: 26000000	-> DDS N: 620370368
0: Freq: 28000000	-> DDS N: 668091200
0: Freq: 30000000	-> DDS N: 715811968
0: Freq: 31000000	-> DDS N: 739672384
*/
// v0.09 2014-10-13  Higher interrupt rate on timer2 and my new encoder ISR state machine work well.
//	- no missing steps even with fast rotation of encoder.  No funny bounces.
//	- seems to be an overflow calculation problem in freq to N conversion when above 10MHz. (Again, crud-code from the net...)
//		@ 25MHz stepping by 1 Hz does not bump up N.  F must change by 4 or 5 Hz, before the values of N changes (jumps large amount).
//		@ 3MHz 1 Hz is 24 N counts.
//		@ 5MHz 1 Hz is toggling between 16, or 32
//
// v0.06 2014-07-10  First crack at rotary encoder function using code from the net
//		- works ok, but not robust on fast turning. And sensitive to bounce.  Need to write my own...
//		Frequency step fixed at 1 Hz
//


// ______________globals for rotary encoder code________________

// Using   'bitRead(PIND,2);' for speed.
#define GET_PHASE_A 	bitRead( PIND, ROT_ENCODER_PHASE_A )
#define GET_PHASE_B 	bitRead( PIND, ROT_ENCODER_PHASE_B )
#define GET_CLICK 		digitalRead( ROT_ENCODER_CLICK )

static volatile int8_t encoderDelta;		// updated by encoder rotate ISR handler.
static volatile int8_t encoderClicked = 0;	// updated by encoder rotate ISR handler.
static int8_t encoderLast;

// Define encoder state machine states
#define WAIT_ANY_EDGE		0
#define WAIT_BOTH_HIGH_1st	1
#define WAIT_BOTH_HIGH_2nd	2
#define DEBOUNCE_A_EDGE		3
#define DEBOUNCE_B_EDGE		4

static int8_t encoderState = WAIT_BOTH_HIGH_1st;	// encoder debounce statemachine.
long encoderTicks = 0;
long rotaryLastReportedSteps = 0;

unsigned long debounceMsecs = 0;			// center click button debounce
static uint8_t ledState = 0;

// ______________variables for frequency________________
#define MHZ_		(1000000UL)
#define KHZ_		   (1000UL)
// Min and max frequency for UI
#define DDS_MAX_FREQ (70UL*MHZ_)
#define DDS_MIN_FREQ (1150UL*KHZ_)

unsigned long startFreq = 10UL * MHZ_;// Power on frequency
unsigned long stopFreq  = 70UL * MHZ_;// Upper limit
unsigned long stepFreq  =  1UL;		// 1Hz steps default

int sweep_dwell = 6200;				// mSecs per step.

// global frequency variable
unsigned long freq = startFreq;		// set power on default (ToDo: replace with value from EEPROM).
unsigned long lastFreq = startFreq;
static int8_t cursorStepFreq = LCD_CURSOR_COL__FREQ_1HZ;	// set to 1Hz position.
unsigned long last_tuning_word = 0;


// AD9851 reference clock in Hertz, for use in 64 bit math (ULL)
#define DDS_CLOCK_FREQ 	(180LL * (1000000LL))
// DDS is about 208Hz too high @ 5MHz.  1700 hz high at 59MHz
int64_t dds_clock_adjust = 5050ll;  // amount in Hertz the 180 MHz DDS clock is off (-Hz if lower, +Hz if higher)
int64_t stepCalibrationFreq  =  2LL;
#define MAX_DDS_OFFSET 8000ll
#define DEF_DDS_OFFSET (5050ll)
#define MIN_DDS_OFFSET -8000ll

// ________________Frequency Presets_____________________

typedef const struct
{
	const unsigned long freq;
	const char* description;
}presets_t;

// define the list of preset frequencies, from issue #1
presets_t presets[]={
	{DDS_MIN_FREQ, ("Lower DDS Limit")},
	{ 1843200ul, ("Baudrate Clk")},
	{ 3276800ul, ("100X 32.768 KHz")},
	{ 3579545ul, ("NTSC Colorburst")},
	{ 7159090ul, ("2X NTSC Colorburst")},
	{10000000ul, ("Std. Reference")},
	{10240000ul, ("PLL & RTC Reference")},
	{11059200ul, ("Baudrate Clk")},
	{14318180ul, ("Baudrate Clk")},
	{22118400ul, ("Baudrate Clk")},
	// International Beacon Project frequencies
	{14100000ul, "14.100 MHz IBP"},
	{18110000ul, "18.110 MHz IBP"},
	{21150000ul, "21.150 MHz IBP"},
	{24930000ul, "24.930 MHz IBP"},
	{28200000ul, "28.200 MHz IBP"},

	{26965000ul, ("CB CH 1")},
	{27125000ul, ("CB CH 14")},
	{27145000ul, ("Toy R/C Channel")},
	{27405000ul, ("CB CH 40")},
	{33333000ul, ("Common CPU Clk")},
	{49860000ul, ("Toy R/C Channel")},
	{DDS_MAX_FREQ,("Upper DDS Limit")},

	{0, (0)}	// end of table.
};
#define PRESET_COUNT	((sizeof(presets)/sizeof(presets_t))-1)

int8_t presetIndex = 1;

// ________________Backlight_Control_____________________

#define BCKLIGHT_PWM_OFF	255
#define BCKLIGHT_PWM_MIN	242	/* one bar */
#define BCKLIGHT_PWM_MID	194
#define BCKLIGHT_PWM_MAX	163	/* 8 bars */

uint8_t backlight_pwm = BCKLIGHT_PWM_MID;

// define some backlight lcd menu control items.
#define BCKLIGHT_LCD_BARS_MAX 8
int8_t backlight_lcd_bars = 5;

#define BCKLIGHT_PWM_STEPS_PER_BAR ((BCKLIGHT_PWM_MIN - BCKLIGHT_PWM_MAX)/(BCKLIGHT_LCD_BARS_MAX-1))
#define backlightBARStoPWM( bars )  (bars==0 ? BCKLIGHT_PWM_OFF : (BCKLIGHT_PWM_MIN - (bars * BCKLIGHT_PWM_STEPS_PER_BAR)))


// -------------------------------------------------------------------
// EEPROM read osc calibration value.
uint8_t eeprom_good;

uint8_t EEPROMgood( void )
{
	uint8_t sig;
	uint8_t ver;
	// check for version sig in eeprom

	sig = EEPROM.read( EEPROM_SIGNATURE_B1 );
	ver = EEPROM.read( EEPROM_SIGNATURE_B2 );
	if( sig == EEPROM_SIGNATURE_VAL && ver == EEPROM_DATA_VERSION )
	{
		eeprom_good = 1;
	}else
	{
		eeprom_good = 0;
		EEPROM.write( EEPROM_SIGNATURE_B1, EEPROM_SIGNATURE_VAL);
		EEPROM.write( EEPROM_SIGNATURE_B2, EEPROM_DATA_VERSION);
	}
	return eeprom_good;
}

void EEPROMreadOscCalibration( void )
{
	int16_t val;
	if( !eeprom_good )
	{
		dds_clock_adjust = DEF_DDS_OFFSET;
		EEPROMwriteOscCalibration();
	}
	val = EEPROM.read( EEPROM_OSC_CALIBRATION_B2 )<<8;
	val |= EEPROM.read( EEPROM_OSC_CALIBRATION_B1 );

	dds_clock_adjust = static_cast<int64_t>( val );
//	dds_clock_adjust = (dds_clock_adjust<<8) | static_cast<int64_t>( val );

}

void EEPROMwriteOscCalibration(void)
{
	int8_t val;
	val = dds_clock_adjust & 0x00ff;
	EEPROM.write( EEPROM_OSC_CALIBRATION_B1, val);
	val = (dds_clock_adjust>>8) & 0x00ff;
	EEPROM.write( EEPROM_OSC_CALIBRATION_B2, val);

}

// -------------------------------------------------------------------
//	Initialization
// -------------------------------------------------------------------
void setup()
{
	eeprom_good = 1;
	// setup serial for debugging
	G_DEBUG(Serial.begin(BAUDRATE));
	// some debug items....
	G_DEBUG(
		Serial.println(F("\n-Start-"));
		// identify the sketch file in case we forget which one the binary was from.
		Serial.println(F("Sketch:") );
		Serial.println(F(__FILE__));
		Serial.print(F("Firmware: "));
		Serial.println(version);
	);

	// set up the LCD's number of columns and rows:
	lcd.begin(20, 2);
	lcd.setCursor(4, 0);
	lcd.print(F("Firmware: "));
	lcd.print(version);


	// MjG:  Setup a RESET pin !!  Very important!
	pinMode(DDS_RESET, OUTPUT);
	digitalWrite(DDS_RESET, HIGH);

	pinMode(DDS_DATA, OUTPUT); 	// sets pin 10 as OUTPUT
	pinMode(DDS_CLOCK, OUTPUT);	// sets pin 9 as OUTPUT
	pinMode(DDS_LOAD, OUTPUT); 	// sets pin 8 as OUTPUT

	// MjG:  Get DDS pins to known state before releasing DDS_RESET.
	digitalWrite(DDS_CLOCK, HIGH);
	digitalWrite(DDS_LOAD, HIGH);
	digitalWrite(DDS_DATA, LOW);

	// get EEPROM data
	eeprom_good = EEPROMgood();
	EEPROMreadOscCalibration();

	backlight_lcd_bars = EEPROM.read( EEPROM_BCKLIGHT_BARS );
	if( (backlight_lcd_bars & 0x80)  && ( backlight_lcd_bars & 0x7f) <= BCKLIGHT_LCD_BARS_MAX )
	{	// should be good EEPROM value if high bit set...
		backlight_lcd_bars &= 0x7f;
	}else
	{
		backlight_lcd_bars = 0x84; // default
		EEPROM.write( EEPROM_BCKLIGHT_BARS, backlight_lcd_bars);
		backlight_lcd_bars &= 0x7f;
		eeprom_good = 0;
	}
	backlight_pwm = backlightBARStoPWM( backlight_lcd_bars );

	pinMode(BCKLIGHT_PWM_PIN, OUTPUT); // back light PWM pin.
	digitalWrite( BCKLIGHT_PWM_PIN, 1);
	analogWrite(BCKLIGHT_PWM_PIN, backlight_pwm);// 255 is off, 225 is dim. 200 is medium. 175 is bright.

	pinMode(LED, OUTPUT);

	//MjG: Now release DDS reset
	digitalWrite(DDS_RESET, LOW);

	// delay to show firmware version
	delay(1800);

	// Print a message to the top line (0) of LCD, starting at column 0.
	lcd.setCursor(0, 0);
	// The F() macro puts the string in Flash memory. Keeps it out of RAM.
	lcd.print(F("  KO0F --  Mark's "));
	lcd.setCursor(0, 1);
	lcd.print(F("DDS Signal Generator"));

	// delay a bit to show our sig.
	delay(2200);

	// clear LCD
	lcd.clear();

	if( !eeprom_good )
	{
		lcd.print(F("EEPROM Error!"));
		delay(2000);
		lcd.clear();
	}
	sendFrequency(0); // first set freq command to DDS seems to be ignored...send all zeros to clear it.
	sendFrequency(freq);
	LCDdisplayFreqLabel();
	LCDdisplayFreq(freq);

	// setup rotary encoder
	QuadratureEncoderInit();
}

// ----------------------------------------------------------------
//
// state machine related declarations
//
// ----------------------------------------------------------------
enum machine_state{
	STARTUP,
	FREQ_CONTROL,
	FREQ_CONTROL_WAIT_UP,
	MENU_SELECT_ENTERING,	// 3

	MENU_SELECT_BACKLIGHT,
	MENU_SELECT_CALIBRATE,
	MENU_SELECT_PRESETS,
	MENU_SELECT_EXIT,		// 7

	MENU_BACKLIGHT,			// must be in the same order as above.
	MENU_CALIBRATE,
	MENU_PRESETS,
	MENU_EXIT,

	MENU_ADD_ONE = 1,
	LAST_STATE
} the_state = FREQ_CONTROL;
machine_state last_state = FREQ_CONTROL;
machine_state next_state = FREQ_CONTROL;
#define MENU_SELECT_COUNT  ( (static_cast<long>(MENU_SELECT_EXIT))  -  (static_cast<long>(MENU_SELECT_ENTERING)) )

enum machine_event{
	NO_EVENT = 0,
	INITIALIZE_STATE,	// used to signal the current state to redisplay if needed.
	ENCODER_CLICK_DOWN,
	ENCODER_CLICK_UP,
	ENCODER_CLICK_LONG,	// held down long time.
	ENCODER_ROTATE,
	TIMEOUT_EVENT
} the_event = INITIALIZE_STATE;
machine_event last_event = NO_EVENT;

unsigned long msec_timer_start = 0;		// used to timeout the menu and exit back to freq...
#define ACTIVITY_TIMEOUT  ( 60UL *1000UL)  /*  mSecs before time out event.... */

// -----------------------------------------------------------------------------

void checkEvent();
void LCDdisplayMenu(machine_state menu);
machine_event returnToState_(machine_state a_state);

unsigned long lastLEDmsec = 0;
// -------------------------------------------------------------------
//	loop
// -------------------------------------------------------------------
void loop()
{
  unsigned long msec_now = millis();
//  unsigned long micros_now = micros();
    machine_event new_event = NO_EVENT;

	// check for event types.
	checkEvent();
	if(the_event == NO_EVENT)
	{
		// LED blinky
		if( msec_now > (lastLEDmsec+300))
		{
			lastLEDmsec = msec_now;
			ledState++;                  // toggle LED
			digitalWrite(LED, ledState&1);
		}
		//  timeout check and event generation here...
		if( msec_timer_start && (msec_now - msec_timer_start) > ACTIVITY_TIMEOUT)
		{
			the_event = TIMEOUT_EVENT;
			msec_timer_start = 0;
		}else
			return;		// nothing to do....
	}
G_DEBUG(
	if((the_event != NO_EVENT && the_event != last_event) || the_event == ENCODER_ROTATE)
	{
	Serial.print(F("State: "));		Serial.print(static_cast<int>(the_state),DEC);
	Serial.print(F(" event: "));	Serial.print(static_cast<int>(the_event),DEC);
	Serial.print(F(" mSec_now: "));	Serial.print((msec_now),DEC);
	Serial.print(F(" debounceMsecs: "));	Serial.print((debounceMsecs),DEC);
	Serial.print(F(" encoderClicked: "));	Serial.print((encoderClicked),DEC);

	Serial.println();
	}
);
	// process event based on current state machine state
	switch(the_state)
	{
		case FREQ_CONTROL:
			next_state = FREQ_CONTROL;
			switch(the_event)
			{
				case INITIALIZE_STATE:
					lcd.clear();
					sendFrequency(freq);
					LCDdisplayFreqLabel();
					LCDdisplayFreq(freq);
				break;

				case ENCODER_ROTATE:
					rotaryLastReportedSteps = encoderTicks;
					lastFreq = freq;
					freq += (rotaryLastReportedSteps * stepFreq);
					if( freq > DDS_MAX_FREQ)
						freq = lastFreq;
					else if ( freq < DDS_MIN_FREQ)
					{
						freq = lastFreq;
					}
					sendFrequency(freq);
					LCDdisplayFreqLabel();
					LCDdisplayFreq(freq);

					lastLEDmsec = msec_now;
					ledState++;                  // toggle LED
					digitalWrite(LED, ledState&1);

					encoderTicks = 0;
					G_DEBUG(
					  Serial.print( F("EncoderTicks:") );
					  Serial.print( rotaryLastReportedSteps, DEC );
					  Serial.println();
					);
				break;

				case ENCODER_CLICK_UP:
					stepFreq *= 100UL;	// bump step size
					cursorStepFreq -= 2;
					if(cursorStepFreq == 3)
						cursorStepFreq = 2; // skip the mhz comma  .
					if(cursorStepFreq == 6)
						cursorStepFreq = 5; // skip the mhz comma  .
					if( stepFreq > MHZ_ )
					{
						stepFreq = 1UL;		// wrap around to 1 Hz.
						cursorStepFreq = LCD_CURSOR_COL__FREQ_1HZ;
					}
					// move the cursor
					LCDsetStepFreqCursor();
				break;

				case ENCODER_CLICK_LONG:
					new_event = INITIALIZE_STATE;
					next_state = MENU_SELECT_ENTERING;
				break;
			}
		break;	// end of freq_control state

		case FREQ_CONTROL_WAIT_UP:
			if(the_event == ENCODER_CLICK_UP)
			{
				new_event = INITIALIZE_STATE;
				next_state = FREQ_CONTROL;
			}else if(the_event == INITIALIZE_STATE)
			{
				lcd.clear();
			}else if(the_event ==  ENCODER_ROTATE)
			{
				encoderTicks = 0;
			}
			break;

		// ========  MENU SYSTEM states below  ==========

		case MENU_SELECT_ENTERING:		// in this state: setup first item of select menu, and wait for UP
			if(the_event == ENCODER_CLICK_UP)
			{
				msec_timer_start = millis();	// timpstamp when we entered menu system.
				next_state = static_cast<machine_state>((1 + (static_cast<long>(next_state)) ));
				LCDdisplayMenu(next_state);		// transition to actual selection menu.
			}else if(the_event == INITIALIZE_STATE)
			{
				LCDdisplayMenu(MENU_SELECT_BACKLIGHT);
			}else if(the_event ==  ENCODER_ROTATE)
			{
					encoderTicks = 0;
			}

			break;

		case MENU_SELECT_BACKLIGHT:
		case MENU_SELECT_CALIBRATE:
		case MENU_SELECT_PRESETS:
			msec_timer_start = millis();		// timpstamp  we did something.
			switch(the_event)
			{
				case ENCODER_CLICK_UP:
					new_event = INITIALIZE_STATE;
					next_state = static_cast<machine_state>(((static_cast<long>(MENU_SELECT_COUNT))
					+ (static_cast<long>(next_state))));
				break;

				case ENCODER_ROTATE:
					menuSelectRotateHandler();
				break;

				case ENCODER_CLICK_LONG:		// exit menu - back to freq control
					new_event = returnToState_(FREQ_CONTROL_WAIT_UP);
				break;
				case TIMEOUT_EVENT:
					new_event = returnToState_(FREQ_CONTROL);
					break;
			}
		break;

		case MENU_SELECT_EXIT:
			switch(the_event)
			{
				case ENCODER_CLICK_DOWN:
				case ENCODER_CLICK_LONG:		// exit menu - back to freq control
					new_event = returnToState_(FREQ_CONTROL_WAIT_UP);
				break;
				case TIMEOUT_EVENT:				// exit menu - back to freq control
					new_event = returnToState_(FREQ_CONTROL);
					break;

				case ENCODER_ROTATE:
					msec_timer_start = millis();	// timpstamp  we did something.
					menuSelectRotateHandler();		// rotate to next menu item.
				break;
			}
		break;

		// ====== Menu action states ======
		case MENU_BACKLIGHT:
			msec_timer_start = millis();	// timpstamp  we did something.
			switch(the_event)
			{
				case INITIALIZE_STATE:
					LCDdisplayMenu(the_state);
				break;

				case ENCODER_ROTATE:
					// calculate a new backlight value based on clicks.
					backlight_lcd_bars += encoderTicks;
					if(backlight_lcd_bars > BCKLIGHT_LCD_BARS_MAX)
						backlight_lcd_bars = BCKLIGHT_LCD_BARS_MAX;
					else if (backlight_lcd_bars < 0)
						backlight_lcd_bars = 0;

					backlight_pwm = backlightBARStoPWM( backlight_lcd_bars );
					analogWrite(BCKLIGHT_PWM_PIN, backlight_pwm);// 255 is off, 225 is dim. 200 is medium. 175 is bright.
					// update LCD
					LCDdisplayMenu(the_state);
					encoderTicks = 0;
				break;

				case ENCODER_CLICK_DOWN:
					// nothing here.  Will save on UP event.
				break;

				case ENCODER_CLICK_LONG:		// exit menu without saving - back to freq control
					new_event = returnToState_(FREQ_CONTROL_WAIT_UP);
				break;

				case ENCODER_CLICK_UP:			// click and release --> exit.
					// save setting on up
					EEPROM.write( EEPROM_BCKLIGHT_BARS, (backlight_lcd_bars | 0x80) );
				case TIMEOUT_EVENT:				// exit menu - back to freq control
					new_event = returnToState_(FREQ_CONTROL);
					break;
			}
		break;

		case MENU_CALIBRATE:
			msec_timer_start = millis();	// timpstamp  we did something.
			switch(the_event)
			{
				case INITIALIZE_STATE:
					LCDdisplayMenu(the_state);
				break;

				case ENCODER_ROTATE:
					rotaryLastReportedSteps = encoderTicks;
					dds_clock_adjust += (rotaryLastReportedSteps * stepCalibrationFreq);
					if( dds_clock_adjust > MAX_DDS_OFFSET)
						dds_clock_adjust = MAX_DDS_OFFSET;
					else if ( dds_clock_adjust < MIN_DDS_OFFSET)
					{
						dds_clock_adjust = MIN_DDS_OFFSET;
					}
					sendFrequency(freq);	// update the output freq based on new calculation with new ref osc.
					LCDdisplayFreq((unsigned long)(DDS_CLOCK_FREQ + dds_clock_adjust));

					encoderTicks = 0;
				break;

				case ENCODER_CLICK_DOWN:
					// nothing here.  Will save on UP event.
				break;

				case ENCODER_CLICK_LONG:		// exit menu without saving - back to freq control
					new_event = returnToState_(FREQ_CONTROL_WAIT_UP);
				break;

				case ENCODER_CLICK_UP:
					EEPROMwriteOscCalibration();// save new value.
				case TIMEOUT_EVENT:				// exit menu - back to freq control
					new_event = returnToState_(FREQ_CONTROL);
					break;
			}
		break;

		case MENU_PRESETS:
			msec_timer_start = millis();	// timpstamp  we did something.
			switch(the_event)
			{
				case INITIALIZE_STATE:
					LCDdisplayMenu(the_state);
				break;

				case ENCODER_ROTATE:
					// Display next preset available.
					presetIndex += encoderTicks;
					if(presetIndex < 0)
					{
						presetIndex = PRESET_COUNT -1;
					}else if(presets[presetIndex].freq == 0)
					{
						presetIndex = 0;
					}
					// update LCD
					LCDdisplayMenu(the_state);
					encoderTicks = 0;
				break;

				case ENCODER_CLICK_DOWN:
					// nothing here.  Will change freq on UP event.
				break;

				case ENCODER_CLICK_LONG:		// exit menu without saving - back to freq control
					new_event = returnToState_(FREQ_CONTROL_WAIT_UP);
				break;

				case ENCODER_CLICK_UP:			// click and release --> exit.
					// frequency setting on up
					freq = presets[presetIndex].freq;
				case TIMEOUT_EVENT:				// exit menu - back to freq control
					new_event = returnToState_(FREQ_CONTROL);
					break;
			}
		break;

		default:			// unknown state
			delay(1200);	// let us see on LCD that something went wrong.
			new_event = returnToState_(FREQ_CONTROL);
		break;
	} //  end of state switch()
	last_state = the_state;
	the_state = next_state;		// transition to next state
	if(the_event != NO_EVENT)
		last_event = the_event;	// save what event we just processed.

	if(new_event != NO_EVENT)
		the_event = new_event;	// set any new event to process next time though the loop().
}

// ===============================================
// set   state machine next_state to requested state..
// stop any running timeout timer.
// Return the event INITIALIZE_STATE
//
machine_event returnToState_(machine_state a_state)
{
	//new_event = INITIALIZE_STATE;
	next_state = a_state;
	msec_timer_start = 0;			// disable timeout.
	encoderTicks = 0;				// make sure no left over rotates
	return(INITIALIZE_STATE);
}

// ===============================================
// handle rotating to the menu selection in response
// to an encoder rotate event.
// input/output: uses global variables
//
// Two rotate clicks before changing to next menu
// to work-around occasional nuisance encoder bounce
// when pressing the encoder select switch.
void menuSelectRotateHandler()
{
	long index_state;
	// require two ticks before changing menus
	if( encoderTicks/2 == 0 )
		return;
	index_state = (static_cast<long>(the_state)) - (static_cast<long>(MENU_SELECT_ENTERING));
	index_state--; // adjust for 0 to N  indexing.
G_DEBUG_N(
Serial.print("\n index_state: ");  		Serial.print(index_state);
Serial.print(" encoderTicks: ");		Serial.print(encoderTicks);
); //G_DEBUG  end

	// divide count by 2 because we require two ticks before changing menu
	rotaryLastReportedSteps = encoderTicks/2;

	if(rotaryLastReportedSteps < 0)
	{
		index_state += (MENU_SELECT_COUNT ) + rotaryLastReportedSteps;
	}
	else
	{
		index_state += rotaryLastReportedSteps;
	}
	index_state %= (MENU_SELECT_COUNT );
	next_state = static_cast<machine_state>(((static_cast<long>(MENU_SELECT_ENTERING)) + 1 + index_state ));

G_DEBUG_N(
Serial.print("\n rotaryLastReportedSteps: ");	Serial.print(rotaryLastReportedSteps);
Serial.print(" index_state: ");  			Serial.print(index_state);
Serial.print("\n the_state: ");  			Serial.print((static_cast<long>(the_state)));
Serial.print(" next_state: ");  Serial.print((static_cast<long>(next_state)));
Serial.println();
);//G_DEBUG  end

	LCDdisplayMenu(next_state);
	encoderTicks = 0;
}

// ===============================================
// check for any new events and set global
// the_event variable as needed.
void checkEvent()
{
	machine_event new_event = NO_EVENT;

	// check if new event was set outside this function
	if( the_event != NO_EVENT && the_event != last_event)
	{
		return;	// pass on the new event from outside...
	}
	// Check if encoder button clicked
	if( encoderClicked == 1 && debounceMsecs > 1500UL*3UL)
	{// check for long click (1.5 seconds)
		new_event = ENCODER_CLICK_LONG;
	}else if( encoderClicked  )
	{
		new_event = ENCODER_CLICK_DOWN;
	}else if( (last_event == ENCODER_CLICK_DOWN || last_event == ENCODER_CLICK_LONG) && !encoderClicked)
	{	// button released
		new_event = ENCODER_CLICK_UP;
	}else
	{
		encoderTicks += QuadratureEncoderCount();
		if ( encoderTicks != 0) {
			new_event = ENCODER_ROTATE;
		}else
		{
			new_event = NO_EVENT;
		}
	}
	// filter multiple events in a row (except ROTATE)
	if(new_event != ENCODER_ROTATE  && new_event == last_event)
	{	// check if new event or same as last time...
		new_event = NO_EVENT;
	}
	the_event = new_event;
}


// ===============================================
// Display the menu
void LCDdisplayMenu(machine_state menu)
{
	char buffer[21];
	lcd.noCursor();
	lcd.clear();
	lcd.setCursor(1, 0);	// col 1, row 0 (top row)
	lcd.print(F("Menu: "));

	lcd.setCursor(1,1);		// col 1, ro 1 (bottom row)
	switch(menu)
	{
		case MENU_SELECT_BACKLIGHT:
		lcd.print(F("Backlight Adj."));
		break;

		case MENU_SELECT_CALIBRATE:
		lcd.print(F("Calibrate Adj."));
		break;

		case MENU_SELECT_PRESETS:
		lcd.print(F("Preset Freq's."));
		break;

		case MENU_SELECT_EXIT:
		lcd.print(F("Exit "));
		break;

		case MENU_BACKLIGHT:
			lcd.print(F(" ["));
			int i;
			for( i = 0; i < BCKLIGHT_LCD_BARS_MAX;i++)
			{
				buffer[i] = (i < backlight_lcd_bars? '=':' ');
			}
			buffer[i] = 0;
			lcd.print(buffer);	// put number of bars for intensity
			lcd.print(F("] "));

			lcd.setCursor(1, 0); // col 1, row 0 (top row)
			lcd.print(F("Backlight: "));
		break;

		case MENU_CALIBRATE:
			lcd.setCursor(1, 0); // col 1, row 0 (top row)
			lcd.print(F("Calibrate: "));

			LCDdisplayFreq((unsigned long)(DDS_CLOCK_FREQ + dds_clock_adjust));
		break;

		case MENU_PRESETS:
			lcd.setCursor(0, 0); // col 0, row 0 (left justify top row)
			lcd.print(presets[presetIndex].description);
			LCDdisplayFreq(presets[presetIndex].freq);
		break;

		default:
			lcd.print(F("Unknown: "));
			lcd.print(static_cast<int>(menu), DEC);
		break;
	}
}
// ==============================================
//
//
void LCDdisplayFreqLabel( void )
{
	char fstring[20];
	lcd.noCursor();
	lcd.setCursor(0, 0);	// col 0, top row
	lcd.print(F("DDS Signal Generator"));
G_DEBUG_N(
// show N count
  lcd.noCursor();
  lcd.setCursor(3, 0);		// col 3, top row.
  sprintf(fstring, "%10lu N",last_tuning_word);
  lcd.print(fstring);
);
}

// ==============================================
//
void LCDdisplayFreq(unsigned long freq )
{
	char fstring[20];

G_DEBUG(
//	last_tuning_word = frequencyToDDSCount_ui64(freq);
    Serial.print("64b: Freq: ");
    Serial.print(freq);
    Serial.print("\t-> DDS N: ");
    Serial.println(last_tuning_word);
);

	sprintf(fstring, "%9lu KHz",freq);
	lcd.noCursor();
    lcd.setCursor(2, 1); // col 2, row 1 (bottom row)
	lcd.print(fstring[0]);
	lcd.print(fstring[1]);
	lcd.print(fstring[2]);
	lcd.print(',');
	lcd.print(fstring[3]);
	lcd.print(fstring[4]);
	lcd.print(fstring[5]);
	lcd.print('.');
    lcd.print(&fstring[6]);
	LCDsetStepFreqCursor();
}

// ===============================================
//
void LCDsetStepFreqCursor()
{
	lcd.cursor();
//	lcd.blink();
	lcd.setCursor(2+cursorStepFreq, 1);
	lcd.cursor();
}

/* *******************************************************
* frequencyToDDSCount_ui64()
* Description:
*  Calculates the 32 bit count (steps) N for the  DDS for the requested frequency.
*
* pow(2, 32) == 4294967296ULL is a constant, so no need to waste CPU cycles
*
*
*
*
* Our DDS has a reference Osc of 30.0006xx MHz
*
*********************************************************/
unsigned long frequencyToDDSCount_ui64(unsigned long freq)
{
	uint64_t tuning_word, frequency;
	frequency = static_cast<uint64_t>(freq);

	tuning_word = (frequency * 4294967296ULL) / (DDS_CLOCK_FREQ + dds_clock_adjust);
	return (static_cast<unsigned long>(tuning_word));
}

/* *******************************************************
*  frequencyToDDSCount(unsigned long frequency)
*
* Description:
*  Calculates the 32 bit count (steps) N for the  DDS for the requested frequency.
*
* Nominal with 30Mhz Osc:
* 	Steps/Hz = 4294967296UL / (180*1000000UL) 	= 23.8609294222
* 	1/(Steps/Hz) = 0.041909515857735564481334514510335
*
* N = F_Hz * 23.8609294222
*
* The fraction part .86092942 in hex is .DC65DEE09B
*
* N = 2147483648  for  90Mhz
* N =  238609294  for  10MHz           hex:  E38E38E
* N = 2386092942  for 100Mhz           hex: 8E38E38E
* N = 2651214377  for 111.111111Mhz    hex: 9E065229
*
* N_int   =         F * 23             111111111 * 23      = 2555555553.
* N_10ths =        (F * 8)/10          111111111 * 8 /10     = 88888888.8
* N_100ths =       (F/10 * 6)/10        11111111 * 6 /10     =  6666666.6
* N_1000ths =      (F/100 * 0)/10        1111111 * 0 /10      =       0.0
* N_10Kths =       (F/1000 * 9)/10        111111 * 9 /10     =    99999.9
* N_100Kths =      (F/10000 * 2)/10        11111 * 2 /10    =      2222.2
* N_1000Kths =     (F/100000 * 9)/10        1111 * 9 /10   =        999.9
* N_10Mths =       (F/1000000 * 4)/10        111 * 4 /10  =          44.4
* N_100Mths =      (F/10000000 * 2)/10        11 * 2 /10 =            2.2
*  - Don't divide each N_xxxths by 10, SUM first, then divide by 10.
* SUM( N_xxxths )/10 + N_int  = 2651214377   WORKS!!
*
* Use divide by 16 math, so convert fraction part radix to base16
* The fraction part .86092942 in hex is .DC65DEE09B
* 111.111111MHz in hex: 69F6BC7                  (add +8  = 69F6BCF)
*
* N_int       =   F * 23 (0x17)   =   9852AEE1
* N_shft8     =  (F  * D)>>4      =   5618791B    56187983
* N_shft4     =  (F>>4 * C)>>4    =    4F790D0
* N_shft8     =  (F>>8 * 6)>>4    =     27BC82
* N_shft12    =  (F>>12 * 5)>>4   =      211CE
* N_shft16    =  (F>>16 * D)>>4   =       5613
* N_shft32    =  (F>>32 * E)>>4   =        5BE
* N_shft36    =  (F>>36 * E)>>4   =         54
*
*   Try 2: with new hex fragment: DC65DEE09B
*  SUM(N_xxxths):  5B3A3460 >>4 =  5B3A346
*        5B3A346   +  9852AEE1 = 9E065227  == 2651214375  (off by -2)
*  Last 6 SUM: 521BB45
*    With F+8 on N_shft8: 56187983  SUM(N_xxxths): 5B3A34C8  >>4 = 5B3A34C
*        5B3A34C   +  9852AEE1 =  9E06522D == 2651214381  (off by +4)
*    With F+8 on N_shft36:  7,1*E = 62  SUM(N_xxxths): 5B3A346E >>4 = 5B3A346
*
*
* max for 32 bit unsigned long var:  0xFFFFFFFF   or  4294967295
*  However the problem is  2^32 is: 0x100000000   or  4294967296
*
*
******************************************************* */
/*
* -----------------------------------------------------------------
* Our DDS has a reference Osc of 30.0006xx MHz
*	Steps/Hz = 4294967296UL / 180003600 		= 23.860452213 177958663048961242997
* Define the multiplier for our osc:
*/
#define OSC_INT		(23UL)
#define OSC_10ths	( 8UL)
#define OSC_100ths	( 6UL)
#define OSC_1Kths	( 0UL)
#define OSC_10Kths	( 4UL)
#define OSC_100Kths	( 5UL)
#define OSC_1Mths	( 2UL)
#define OSC_10Mths	( 2UL)
#define OSC_100Mths	( 1UL)
#define OSC_1Bths	( 3UL)

unsigned long frequencyToDDSCount(unsigned long frequency)
{

	unsigned long Fo = frequency;		// frequency of desired operation
	unsigned long N_int = 999999;		// integer part of DDS count
	unsigned long N_fracAccu = 0;		// fractional accumulator of DDS count

	/* no calibration yet.
	long F_offset;
	// first calibrate F to DSS offset slope.
	Fo = Fo/10000UL;
	F_offset = ((Fo * F_errorSlope)/100UL) + F_errorIntercept;
	Fo = frequency - F_offset;
	*/

	//Fo += F_calibrate;		// add or subtract calibration correction.

	N_int = Fo * OSC_INT;		// get integer part of N.
	// perform fractional part, fraction of scaler is: .860452213  or whatever is in OSC_xxxths
	N_fracAccu = Fo * OSC_10ths;
	Fo = Fo / 10UL;
	N_fracAccu += Fo * OSC_100ths;
	Fo = Fo / 10UL;
	N_fracAccu += Fo * OSC_1Kths;
	Fo = Fo / 10UL;
	N_fracAccu += Fo * OSC_10Kths;
	Fo = Fo / 10UL;
	N_fracAccu += Fo * OSC_100Kths;
	Fo = Fo / 10UL;
	N_fracAccu += Fo * OSC_1Mths;
	Fo = Fo / 10UL;
	N_fracAccu += Fo * OSC_10Mths;
	Fo = Fo / 10UL;
	N_fracAccu += Fo * OSC_100Mths;
	Fo = Fo / 10UL;
	N_fracAccu += Fo * OSC_1Bths;

    return (N_int + (N_fracAccu/10UL));
}

// -----------------------------------------------------------
//
void sendFrequency(unsigned long frequency)
{
	unsigned long tuning_word;

	digitalWrite(DDS_LOAD, LOW);

	//tuning_word = frequencyToDDSCount( frequency );
	tuning_word = frequencyToDDSCount_ui64( frequency );
	last_tuning_word = tuning_word;	// save a copy for LCD display use.
	// Send 32 bit (4 byte) tuning word (DDS Count)
	for(int i = 0; i < 4; i++)
	{
		ByteOut( tuning_word );
		tuning_word = tuning_word >> 8;
	}
	// Send command byte:
	// D7 Phase-b4 (MSB)  Phase: 11.25 deg / step.  * 32 steps = 360 deg.
	// D6 Phase-b3
	// D5 Phase-b2
	// D4 Phase-b1
	// D3 Phase-b0 (LSB)
	// D2 Power-Down  (1 down, 0 up)
	// D1 Logic 0*
	// D0 6X REFCLK Multiplier Enable
	//byte_out(0x09);	// command byte. Phase set to 11.25 deg.
	ByteOut(0x01);		// MjG: We want 0 deg phase! and 6X refclk.

	digitalWrite (DDS_LOAD, HIGH); // Take DDS load pin high to set new freq.
}

void BitOut(byte bit_v)
{  // mjg: output the LSB
	digitalWrite(DDS_CLOCK, LOW);
	digitalWrite(DDS_DATA, bit_v & 1);
	digitalWrite(DDS_CLOCK, HIGH);
}

void ByteOut( byte byte_v)
{
	int i;

	for (i = 0; i < 8; i++)
	{
		digitalWrite(DDS_CLOCK, LOW);
		digitalWrite(DDS_DATA, byte_v & 1);
		digitalWrite(DDS_CLOCK, HIGH);
		byte_v = byte_v >> 1;
	}
}




/* ===============================================
ISR( TIMER2_COMPA_vect )

about every 290 uSecs this fires.

Encoder signal:
CW:
A ___        _______
     |______|

B ______        _____
        |______|
    2  1  0  3   2  2   val
    2  2  1  0   3  2   encoderLast
    0  1 -1 -3   1  0   diff
    +            +      Detents in encoder.

Encoder signal:
CCW:
A ______        _______
        |______|

B ___        _____
     |______|
    2  3  0  1   2  2   val
    2  2  3  0   1  2   encoderLast
    0  1  3 -1  -1  0   diff
    +            +      Detents in encoder.

	Note: the variables and values are based on the original authors method.
	I changed it since my encoders have a detent and always go through the 4 states on one detent click.
	I don't need to know the sub states - only need to know which phase started first and when both are back HIGH (idle).

*/
// declare Timer2 Compare A interrupt handler.
ISR( TIMER2_COMPA_vect )
{
	int8_t val, diff;
	switch(encoderState)
	{
	case WAIT_BOTH_HIGH_1st:
	  if( GET_PHASE_A && GET_PHASE_B )
		encoderState = WAIT_BOTH_HIGH_2nd;
	  else
		encoderState = WAIT_BOTH_HIGH_1st;		// wait for detent home position...
	break;
	case WAIT_BOTH_HIGH_2nd:
		if( GET_PHASE_A && GET_PHASE_B )
		{
			encoderState = WAIT_ANY_EDGE;		// prepare for another edge.
			encoderLast =  2;
		}
		else
			encoderState = WAIT_BOTH_HIGH_1st;	// Still bouncing.
	break;

    case WAIT_ANY_EDGE:

		if( !GET_PHASE_A )
			encoderState = DEBOUNCE_A_EDGE;
		else if ( !GET_PHASE_B )
			encoderState = DEBOUNCE_B_EDGE;
		break;

	case DEBOUNCE_A_EDGE:
	case DEBOUNCE_B_EDGE:
		if( (encoderState == DEBOUNCE_A_EDGE && GET_PHASE_A) ||
			(encoderState == DEBOUNCE_B_EDGE && GET_PHASE_B))
		{	// bouncing....
			encoderState = WAIT_ANY_EDGE;
			encoderLast =  2;
			break;
		}

		val = 0;
		if( GET_PHASE_A )
			val = 3;
		if( GET_PHASE_B )
			val ^= 1;	// convert gray to binary
		diff = encoderLast - val;	// difference last - new
		if( diff & 1 )
		{ // bit 0 = value (1)
			encoderLast = val;		// store new as next last
			encoderDelta += (diff & 2) - 1; // bit 1 = direction (+/-)  1=+ 0=-
			encoderState = WAIT_BOTH_HIGH_1st;
		}
	break;
	}

	// check for encoder push button clicked (== 0)
	if( !GET_CLICK )
	{	// button clicked
		debounceMsecs++;
		if( debounceMsecs > (55UL*3UL))
			encoderClicked = 1;
	}else if( encoderClicked && debounceMsecs < (55UL*3UL))
	{ // debounce button released.
		debounceMsecs--;
		if( debounceMsecs < 3UL)
			encoderClicked = 0;
	}else if( encoderClicked )
	{	// was clicked, now just released, start up debounce.
		debounceMsecs = (54UL*3UL);
	}else if( !encoderClicked )
	{	// reset debounce time
		debounceMsecs = 0;
	}
}

// ===================================================================
// setup timer2 for compare
// Timer2:
// Timer2 is an 8bit timer like Timer0.
// In the Arduino the tone() function uses Timer2.
// Timer2 is initialized by default to a frequency of 980 Hz (~1ms period)
// We need it to be faster (3x faster)
void QuadratureEncoderInit(void)
{
	int8_t val;

	pinMode( ROT_ENCODER_CLICK ,INPUT);  // pushbutton on encoder
	digitalWrite( ROT_ENCODER_CLICK, 1); // enable pull-up.

	pinMode( ROT_ENCODER_PHASE_A, INPUT);
	pinMode( ROT_ENCODER_PHASE_B, INPUT);
        digitalWrite( ROT_ENCODER_PHASE_A, HIGH);	// turn on pull-up resistor
	digitalWrite( ROT_ENCODER_PHASE_B, HIGH);		// turn on pull-up resistor

	val=0;
	if (GET_PHASE_A)
		val = 3;
	if (GET_PHASE_B)
		val ^= 1;
	encoderLast = val;
	encoderDelta = 0;
	encoderTicks = 0;

	cli();
// initialize Timer2 to 290 uSec rate.

	TCCR2A = 1;  // needs to be 1 (WGM20) for OCR2A to have an effect. WGM21 = 0.
	TCCR2B = 0;  // not setting 0 here causes very low frequency. WGM23, WGM22 = 0.  CS2 x = 000
	//TCNT2 = 0;

	TIMSK2 = (1 << OCIE2A);	// enable Compare Match A interrupt.

	// OCR2A has no effect if TCCR2A is zero.
	OCR2A = 76; // compare match register 16MHz/64/3.25KHz

	TCCR2B |= (1 << WGM22);  // CTC mode  Really: WGM22 combined with WGM20 = 1 give Fast PWM mode - which works.  CTC does not seem to work.
	TCCR2B |= (1 << CS21) | (1 << CS20); // 64 prescaler

	sei();
}

// ======================================================
// get accumulated encoder ticks since last call.
int8_t QuadratureEncoderCount( void )
{
	int8_t val;

	cli();
	val = encoderDelta;
	encoderDelta = 0;
	sei();
	return val; // counts since last call
}



// ______________________________Some_notes_&_example_code_below____________________________________________________

// using the F()  macro which keeps a string in Flash instead of copying to RAM on this annoying AVR micro.
// example to be able to call:
//  debugWrite(F("Mark's device. V1.0");
// debugWrite(F("PROGMEM string");
// debugWrite("another RAM string");
/*
void debugWrite( __FlashStringHelper* data) {
  if ( debugWriteFlag == 1 || debugWriteFlag == 3 ) {
    lcd.print(data);
  }
  if ( debugWriteFlag == 2 || debugWriteFlag == 3 ) {
    Serial.println(data);
  }
}
void debugWrite(String data) {
  if ( debugWriteFlag == 1 || debugWriteFlag == 3 ) {
    lcd.print(data);
  }
  if ( debugWriteFlag == 2 || debugWriteFlag == 3 ) {
    Serial.println(data);
  }
}
*/
// Also use PROGMEM type modifier:
// see http://www.arduino.cc/en/Reference/PROGMEM
/*
#include <avr/pgmspace.h>
PROGMEM prog_char string_0[] = "String 0";   // "String 0" etc are strings to store - change to suit.

char buffer[30];    // make sure this is large enough for the largest string it must hold


strcpy_P(buffer, (char*)pgm_read_word(&(string_0[0]))); // Necessary casts and dereferencing, just copy.

*/
